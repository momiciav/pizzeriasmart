package com.lezjava.pizzeria.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table (name="Ricetta")
public class Ricetta {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @Column(name = "nome")
  private String nome;

  @Column(name = "esecuzione")
  private String esecuzione;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(name = "ricette_ingredienti", joinColumns = {@JoinColumn(name = "ricetta_id")},
      inverseJoinColumns = {@JoinColumn(name = "ingrediente_id")})
  Set<Ingrediente> ingredienti = new HashSet<>();

}
