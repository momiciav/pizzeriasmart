package com.lezjava.pizzeria.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "Ingrediente")
public class Ingrediente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "categoria")
	private String categoria;

	@Column(name = "quantita")
	private double quantita;

	@Column(name = "prezzo")
	private double prezzo;

 @ManyToMany(mappedBy = "ingredienti")
private Set<Ricetta> ricette = new HashSet<>();

	}
