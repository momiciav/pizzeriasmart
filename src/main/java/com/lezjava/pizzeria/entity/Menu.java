package com.lezjava.pizzeria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table (name="Menu")
public class Menu {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @Column(name = "nome")
  private String nome;

  @Column(name = "tipo")
  private String tipo;

  @Column(name = "categoria")
  private String categoria;

  @Column(name = "peso")
  private double peso;

  @Column(name = "prezzo_prod")
  private float prezzoProd;

	
}



