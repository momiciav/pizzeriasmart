package com.lezjava.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lezjava.pizzeria.dto.IngredienteDto;
import com.lezjava.pizzeria.entity.Ingrediente;
import com.lezjava.pizzeria.repository.IngredienteRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IngredienteService {

	@Autowired
	IngredienteRepository ingredienteRepo; /* == IngredienteRepository ingRepo = new IngredienteRepository() */

	public void salvaIngrediente(IngredienteDto ingredienteDto) {
		try {
			Ingrediente ingrediente = new Ingrediente();

			ingrediente.setCategoria(ingredienteDto.getCategoria());
			ingrediente.setNome(ingredienteDto.getNome());
			ingrediente.setPrezzo(ingredienteDto.getPrezzo());
			ingrediente.setQuantita(ingredienteDto.getQuantita());
			ingredienteRepo.save(ingrediente);
			log.debug("ingrendiente con id: " + ingrediente.getId() + " salvato con successo");

		} catch (Exception e) {
			log.debug("errore nel salvatagio di questo ingrediente: " + e.getMessage());
		}

	}

	public void salvaIngredienti(IngredienteDto[] ingredientiDto) {
		try {
			for (int i = 0; i < ingredientiDto.length; i++) {
				salvaIngrediente(ingredientiDto[i]);
			}
			log.debug("ingrendienti salvati con successo");
		} catch (Exception e) {
			log.debug("errore nel salvataggio della lista ingredienti: " + e.getMessage());
		}

	}

	public IngredienteDto getIngrediente(Long id) {
		try {
			Ingrediente entity = ingredienteRepo.findById(id).get();

			IngredienteDto dto = new IngredienteDto();

			dto.setCategoria(entity.getCategoria());
			dto.setNome(entity.getNome());
			dto.setPrezzo(entity.getPrezzo());
			dto.setQuantita(entity.getQuantita());
			log.debug("ingrediente con id:" + id + "trovato con success");
			return dto;
		} catch (Exception e) {
			log.debug("errore nella richesta di prendere questo ingrediente: " + e.getMessage());
			return null;
		}
	}

	public String deleteIngrediente(Long id) {
		try {
			ingredienteRepo.deleteById(id);
			log.debug("ingrendiente con id: " + id + " cancellato con successo");
			return "ingrediente cancellato";
		} catch (Exception e) {
			log.debug("errore nella cancellazione: " + e.getMessage());
			return "errore nella cancellazione: " + e.getMessage();
		}
	}

	public List<Ingrediente> getAllIngredienti() {
		try {
			List<Ingrediente> resultList = ingredienteRepo.findAll();
			return resultList;
		} catch (Exception e) {
			log.debug(("errore nel recupero della lista: " + e.getMessage()));
			return null;
		}
	}

}