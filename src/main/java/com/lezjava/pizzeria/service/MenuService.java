package com.lezjava.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lezjava.pizzeria.dto.MenuDto;
import com.lezjava.pizzeria.entity.Menu;
import com.lezjava.pizzeria.repository.MenuRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MenuService {

	@Autowired
	MenuRepository menuRepo; /* == MenuRepository menuRepo = new MenuRepository() */

	public void salvaMenu(MenuDto menuDto) {
		try {
			Menu menu = new Menu();
			menu.setNome(menuDto.getNome());
			menu.setTipo(menuDto.getTipo());
			menu.setCategoria(menuDto.getCategoria());
			menu.setPeso(menuDto.getPeso());
			menu.setPrezzoProd(menuDto.getPrezzoProd());
			menuRepo.save(menu);
			log.debug("menu con id: " + menu.getId() + " salvato con successo");
			
		} catch (Exception e) {
			log.debug("errore nella salvazione di questo ingrediente: " + e.getMessage());
		}
	}

	public MenuDto getMenu(Long id) {
		try {
			Menu entity = menuRepo.findById(id).get();

			MenuDto dto = new MenuDto();

			dto.setNome(entity.getNome());
			dto.setTipo(entity.getTipo());
			dto.setCategoria(entity.getCategoria());
			dto.setPeso(entity.getPeso());
			dto.setPrezzoProd(entity.getPrezzoProd());
			log.debug("menu con id:" + id + "trovato con success");
			return dto;
		} catch (Exception e) {
			log.debug("errore nella richesta di prendere questo menu: " + e.getMessage());
			return null;
		}
	}
	
	public void salvaMenus(MenuDto[] MenusDto) {
		try {
			for (int i = 0; i < MenusDto.length; i++) {
				salvaMenu(MenusDto[i]);
			}
			log.debug("Menus salvati con successo");
		} catch (Exception e) {
			log.debug("errore nel salvataggio della lista di menus: " + e.getMessage());
		}

	}
	
	public List<Menu> getAllMenus() {
		try {
			List<Menu> resultList = menuRepo.findAll();
			return resultList;
		} catch (Exception e) {
			log.debug(("errore nel recupero della lista: " + e.getMessage()));
			return null;
		}
	}
	
	public String deleteMenu(Long id) {
		try {
			menuRepo.deleteById(id);
			log.debug("menu con id: " + id + " cancellato con successo");
			return "menu cancellato";
		} catch (Exception e) {
			log.debug("errore nella cancellazione: " + e.getMessage());
			return "errore nella cancellazione: " + e.getMessage();
		}
	}
	}
