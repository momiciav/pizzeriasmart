package com.lezjava.pizzeria.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lezjava.pizzeria.dto.RicettaDto;
import com.lezjava.pizzeria.entity.Ricetta;
import com.lezjava.pizzeria.repository.RicettaRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RicettaService {

	@Autowired
	RicettaRepository ricettaRepo; /* == RicettaRepository ricettaRepo = new RicettaRepository() */

	public void salvaRicetta(RicettaDto ricettaDto) {
		try {
			Ricetta ricetta = new Ricetta();
			ricetta.setNome(ricettaDto.getNome());
			ricetta.setEsecuzione(ricettaDto.getEsecuzione());
			
			ricettaRepo.save(ricetta);
			log.debug("ricetta con id: " + ricetta.getId() + " salvato con successo");
			
		} catch (Exception e) {
			log.debug("errore nella salvazione di questa ricetta: " + e.getMessage());
		}
	}
}

/*
	public MenuDto getMenu(Long id) {
		try {
			Menu entity = menuRepo.findById(id).get();
			
			MenuDto dto = new MenuDto();

			dto.setNome(entity.getNome());
			dto.setTipo(entity.getTipo());
			dto.setCategoria(entity.getCategoria());
			dto.setPeso(entity.getPeso());
			dto.setPrezzoProd(entity.getPrezzoProd());
			log.debug("menu con id:" + id + "trovato con success");
			return dto;
		} catch (Exception e) {
			log.debug("errore nella richesta di prendere questo menu: " + e.getMessage());
			return null;
		}
	}
		
	public void salvaMenus(MenuDto[] MenusDto) {
		try {
			for (int i = 0; i < MenusDto.length; i++) {
				salvaMenu(MenusDto[i]);
			}
			log.debug("Menus salvati con successo");
		} catch (Exception e) {
			log.debug("errore nel salvataggio della lista di menus: " + e.getMessage());
		}

	}
	
	public List<Menu> getAllMenus() {
		try {
			List<Menu> resultList = menuRepo.findAll();
			return resultList;
		} catch (Exception e) {
			log.debug(("errore nel recupero della lista: " + e.getMessage()));
			return null;
		}
	}
	
	public String deleteMenu(Long id) {

		try {
			menuRepo.deleteById(id);
			log.debug("menu con id: " + id + " cancellato con successo");
			return "menu cancellato";
		} catch (Exception e) {
			log.debug("errore nella cancellazione: " + e.getMessage());
			return "errore nella cancellazione: " + e.getMessage();
		}
	}
	*/
	
