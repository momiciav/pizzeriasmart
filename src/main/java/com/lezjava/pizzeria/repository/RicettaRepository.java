package com.lezjava.pizzeria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lezjava.pizzeria.entity.Menu;
import com.lezjava.pizzeria.entity.Ricetta;

@Repository
public interface RicettaRepository extends JpaRepository<Ricetta, Long> {

	Menu findByNome(String nome);

}
