package com.lezjava.pizzeria.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RicettaDto {
	
	 private Long id;
	 private String nome;
	 private String esecuzione;

}
