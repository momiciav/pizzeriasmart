package com.lezjava.pizzeria.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MenuDto {

	private Long id;
	private String nome;
	private String tipo;
	private String categoria;
	private double peso;
	private float prezzoProd;
}
