package com.lezjava.pizzeria.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredienteDto {

	private Long id;
	private String nome;
	private String categoria;
	private double quantita;
	private double prezzo;
}
