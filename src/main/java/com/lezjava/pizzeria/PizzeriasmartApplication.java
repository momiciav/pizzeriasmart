package com.lezjava.pizzeria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzeriasmartApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzeriasmartApplication.class, args);
	}

}
