package com.lezjava.pizzeria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lezjava.pizzeria.dto.RicettaDto;
import com.lezjava.pizzeria.service.RicettaService;

@RestController
public class RicettaController {

	@Autowired
	RicettaService ricettaService;

	@PostMapping("/salva-ricetta")
	public String salvaRicetta(@RequestBody RicettaDto dto) {
		ricettaService.salvaRicetta(dto);
		return "Ricetta salvata";
	}
/*	
	@GetMapping("/find-menu")
	public MenuDto getMenu(@RequestParam Long id) {
		MenuDto response = menuService.getMenu(id);
		return response;
	}
	@PostMapping("/salva-menus")
	public String salvaMenus(@RequestBody MenuDto[] dtos) {
		menuService.salvaMenus(dtos);
		return "menus salvati";
	}
	@GetMapping("/find-all-menus")
	public List<Menu> getAllMenus (){		
		return menuService.getAllMenus(); 
	}
	@GetMapping ("/delete-menu")
	public String deteleMenu (@RequestParam Long id) {
		String result = menuService.deleteMenu(id);
			return result;
	}
	*/
}