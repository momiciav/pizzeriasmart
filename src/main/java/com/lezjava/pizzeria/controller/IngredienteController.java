package com.lezjava.pizzeria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lezjava.pizzeria.dto.IngredienteDto;
import com.lezjava.pizzeria.entity.Ingrediente;
import com.lezjava.pizzeria.service.IngredienteService;

@RestController
public class IngredienteController {

	@Autowired
	IngredienteService ingredienteService;

	@PostMapping("/salva-ingrediente")
	public String salvaIngrediente(@RequestBody IngredienteDto dto) {
		ingredienteService.salvaIngrediente(dto);
		return "ingrediente salvato";
	}
	@PostMapping("/salva-ingredienti")
	public String salvaIngredienti(@RequestBody IngredienteDto[] dtos) {
		ingredienteService.salvaIngredienti(dtos);
		return "ingredienti salvati";
	}

	@GetMapping("/find-ingrediente")
	public IngredienteDto getIngrediente(@RequestParam Long id) {
		IngredienteDto response = ingredienteService.getIngrediente(id);
		return response;
	}

	@GetMapping("/delete-ingrediente")
	public String deleteIngrediente(@RequestParam Long id) {
		String result = ingredienteService.deleteIngrediente(id);
		return result;
	}
	
	@GetMapping("/find-all-ingrediente")
	public List<Ingrediente> getAllIngredienti (){		
		return ingredienteService.getAllIngredienti(); 
	}
}
