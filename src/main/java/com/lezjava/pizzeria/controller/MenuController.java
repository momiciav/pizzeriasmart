package com.lezjava.pizzeria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lezjava.pizzeria.dto.MenuDto;
import com.lezjava.pizzeria.entity.Menu;
//import com.lezjava.pizzeria.entity.Ingrediente;
import com.lezjava.pizzeria.service.MenuService;

@RestController
public class MenuController {

	@Autowired
	MenuService menuService;

	@PostMapping("/salva-menu")
	public String salvaMenu(@RequestBody MenuDto dto) {
		menuService.salvaMenu(dto);
		return "Menu salvato";
	}
	
	@GetMapping("/find-menu")
	public MenuDto getMenu(@RequestParam Long id) {
		MenuDto response = menuService.getMenu(id);
		return response;
	}
	@PostMapping("/salva-menus")
	public String salvaMenus(@RequestBody MenuDto[] dtos) {
		menuService.salvaMenus(dtos);
		return "menus salvati";
	}
	@GetMapping("/find-all-menus")
	public List<Menu> getAllMenus (){		
		return menuService.getAllMenus(); 
	}
	@GetMapping ("/delete-menu")
	public String deteleMenu (@RequestParam Long id) {
		String result = menuService.deleteMenu(id);
			return result;
	}
}